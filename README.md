# Brew Master Log

For Development Environment on Unix:
1. On the terminal type:

    export ASPNETCORE_ENVIRONMENT=Development

Add your Brewery DB API Key also in the environment:
1. On the terminal type:

    export BeerApiKey=THE_KEY_GOES_IN_HERE_SO_REPLACE_THIS_TEXT

To print a list of environment variables type:
 
    printenv

Or use the Secrets tool to the the secret:
(Never used this before so I don't know if you need
to added it or it is already added):
1. On the terminal in the src folder of the project type:

    dotnet user-secrets set BeerApiKey THE_KEY_GOES_IN_HERE_SO_REPLACE_THIS_TEXT

For more info on User Secrets: [Safe storage of app secrets during development](https://docs.asp.net/en/latest/security/app-secrets.html)


## This application consists of: