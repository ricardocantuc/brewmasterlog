using System.ComponentModel.DataAnnotations;

namespace WHWebApp.ViewModels.AccountViewModels
{    
        public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    
    }
}