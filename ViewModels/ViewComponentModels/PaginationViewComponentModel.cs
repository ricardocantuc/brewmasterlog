namespace WHWebApp.ViewModels.ViewComponentModels
{
    public class PaginationViewComponentModel
    {
        public int PageNumber { get; set; }

        public string Controller { get; set; }
        
        public string Action { get; set; }

        public int MaxPageNumber { get; set; }

        public string SearchName { get; set; }
    }
}