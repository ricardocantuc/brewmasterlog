using System.ComponentModel.DataAnnotations;
using WHWebApp.Models;

namespace WHWebApp.ViewModels.LocationsViewModels
{     
    public class IndexViewModel
    {        
        public string FirstName { get; set; }
        
        [StringLength(100,MinimumLength = 3,ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.")]
        public string City { get; set; }

        public string GoogleSrc { get; set; }

        [Display(Name = "State")]
        [Required]
        public StatesEnum EnumState { get; set; }           
    }        
}