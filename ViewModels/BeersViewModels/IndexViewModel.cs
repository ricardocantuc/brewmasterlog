using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WHWebApp.Models;

namespace WHWebApp.ViewModels.BeersViewModels
{
    public class IndexViewModel
    {
        public IEnumerator<Beer> Beers { get; set; }

        public string FirstName { get; set; }

        [Required(ErrorMessage="Beer name is required")]
        [StringLength(100,MinimumLength = 3,ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.")]
        public string Search { get; set; }

    }
}