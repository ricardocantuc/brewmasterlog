using WHWebApp.Models;

namespace WHWebApp.ViewModels.BeersViewModels
{
    public class BeerInfoViewModel
    {
        public Beer Beer { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }
    }
}
