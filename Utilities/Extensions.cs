using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using WHWebApp.Models;


namespace WHWebApp.Utilities
{
    public class Extensions
    {
        public static string IsActive(ViewContext viewContext, string controller, string action)
        {
            var actionRouteData = viewContext.RouteData.Values["action"].ToString();
            var controllerRouteData = viewContext.RouteData.Values["controller"].ToString();
            return ((actionRouteData == action) && (controllerRouteData == controller)) ? "active" : "";
        }

        public static string IsActive(ViewContext viewContext, string controller)
        {            
            var controllerRouteData = viewContext.RouteData.Values["controller"].ToString();
            return (controllerRouteData == controller) ? "active" : "";
        }

        public static string IsActiveDropdown(ViewContext viewContext, string controller, string[] actions)
        {
            var actionRouteData = viewContext.RouteData.Values["action"].ToString();
            var controllerRouteData = viewContext.RouteData.Values["controller"].ToString();

            var activeAction = false;

            foreach (var action in actions)
            {
                if (actionRouteData == action)
                {
                    activeAction = true;
                    break;
                }
            }

            return (activeAction && (controllerRouteData == controller)) ? "active" : "";
        }

        public static async Task<string> GetUserFullName(string userName, UserManager<ApplicationUser> manager)
        {   
            try
            {
                var user = await manager.FindByNameAsync(userName);
                var fullName = user.FirstName + " " + user.LastName;
                return fullName;
            }
            catch
            {
                return userName;
            }
        }

        public static async Task<string> GetUserFirstName(string userName, UserManager<ApplicationUser> manager)
        {   
            try
            {
                var user = await manager.FindByNameAsync(userName);
                var firstName = user.FirstName;
                return firstName;
            }
            catch
            {
                return userName;
            }
        }
        
        public static string SanitizeString(string strToSanitize)
        {            
            //For now it only trims the string other stuff may need to be done
            if(!string.IsNullOrEmpty(strToSanitize)){
                var value = strToSanitize.Trim();

                return value;
            }

            return null;
        }

        public static string LocationSearch(string city, string state)
        {
            city = SanitizeString(city);
            
            city = string.IsNullOrEmpty(city) ? "" : city + ", ";
            
            return city + SanitizeString(state);
        }

        public static string GetRegion(StatesEnum stateEnum)
        {
            switch(stateEnum)
            {
                case StatesEnum.NewHampshire:
                    return "New Hampshire";
                case StatesEnum.NewJersey:
                    return "New Jersey";
                case StatesEnum.NewMexico:
                    return "New Mexico";
                case StatesEnum.NewYork:
                    return "New York";
                case StatesEnum.NorthCarolina:
                    return "North Carolina";
                case StatesEnum.NorthDakota:
                    return "North Dakota";
                case StatesEnum.RhodeIsland:
                    return "Rhode Island";
                case StatesEnum.SouthCarolina:
                    return "South Carolina";
                case StatesEnum.SouthDakota:
                    return "South Dakota";
                case StatesEnum.WestVirginia:
                    return "West Virginia";
                default:
                    return stateEnum.ToString("G");
            }
        }        
    }
}
 