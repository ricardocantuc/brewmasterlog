using WHWebApp.Models;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace WHWebApp.Services
{
    public class BeerApiService
    {
        private readonly string NO_IMG = "/img/nobrewerypic.jpg";
        private readonly string NOT_AVAILABLE = "N/A";
        private string _key;
        private string _url;

        private ILogger _logger;
        
        public BeerApiService(ILoggerFactory loggerFactory)
        {
            _url = Startup.Configuration["BeerApiURL"];
            _key = Startup.Configuration["BeerApiKey"];
            _logger = loggerFactory.CreateLogger<BeerApiService>();
        }

        private async Task<JObject> GetJsonFromApi(string searchQuery)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync(SearchUrl(searchQuery));

            _logger.LogInformation(20,SearchUrl(searchQuery));
            return JObject.Parse(json);
        }
        
        private string SearchUrl(string searchQuery)
        {         
            return _url + searchQuery + _key;
        }
               
        private string StringFromJObjectInJToken(JToken jToken, string objKey, string valueKey, bool isImage)
        {
            var returnValue = string.Empty;
            try
            {                
                returnValue = jToken.Value<JToken>(objKey).Value<string>(valueKey);
            }
            catch
            {                
                returnValue = isImage ? NO_IMG : NOT_AVAILABLE;
            }
                        
            return Utilities.Extensions.SanitizeString(returnValue);
        }
       
        private string StringFromJToken(JToken jToken, string valueKey)
        {
            
            var returnValue = jToken.Value<string>(valueKey) ?? NOT_AVAILABLE;
            
            return Utilities.Extensions.SanitizeString(returnValue);
        }
        
        public async Task<BeerApiServiceResult> GetBeerInformation(string beerId)
        {          
            var result = new BeerApiServiceResult()
            {
                Success = false,
                Message = "Undetermined failure while looking up for beer information!"
            };
            
            var results = await GetJsonFromApi($"beer/{beerId}?withBreweries=Y&key=");
            
            if (results.Value<string>("status") != "success")
            {
                return result;
            }

            JToken beerData;

            try
            {              
                beerData = results.Value<JToken>("data");
            }
            catch
            {
                return result;
            }

            JToken breweryData;
            try
            {   
                breweryData = beerData.Value<JArray>("breweries")[0];
            }
            catch 
            {
                breweryData = null;           
            }            
              
            result.Beer = new Beer()
            {
                Name = StringFromJToken(beerData,"name"),
                IdApi = StringFromJToken(beerData,"id"),
                Label = StringFromJObjectInJToken(beerData, "labels", "large", true),
                Icon = StringFromJObjectInJToken(beerData, "labels", "icon", true),
                Abv = StringFromJToken(beerData,"abv"),
                Ibu = StringFromJToken(beerData,"ibu"),
                GlassName = StringFromJObjectInJToken(beerData,"glass", "name", false),
                IsOrganic = beerData.Value<string>("isOrganic") == "Y" ? "Yes" : "No" ?? "N/A",
                Description = StringFromJToken(beerData,"description"),
                Brewery = new Brewery
                {
                    Name = StringFromJToken(breweryData,"nameShortDisplay"),
                    Banner = StringFromJObjectInJToken(breweryData,"images","large",true),
                    Website = StringFromJToken(breweryData,"website"),
                    Established = StringFromJToken(breweryData,"established"),
                    IdApi = StringFromJToken(breweryData,"id"),
                    Description = StringFromJToken(breweryData,"description"),
                    Image = StringFromJObjectInJToken(breweryData,"images","squareMedium",true)
                },
                Availability = new Availability
                {
                    Name = StringFromJObjectInJToken(beerData, "available", "name", false),
                    Description = StringFromJObjectInJToken(beerData, "available", "description", false),
                    IdApi = StringFromJObjectInJToken(beerData, "available", "id", false)
                },
                Style = new Style
                {
                    IdApi = StringFromJObjectInJToken(beerData, "style", "id", false),
                    Name = StringFromJObjectInJToken(beerData, "style", "name", false),
                    ShortName = StringFromJObjectInJToken(beerData, "style", "shortName", false),
                    Description = StringFromJObjectInJToken(beerData, "style", "description", false),
                    IbuMin = StringFromJObjectInJToken(beerData, "style", "ibuMin", false),
                    IbuMax = StringFromJObjectInJToken(beerData, "style", "ibuMax", false),
                    AbvMin = StringFromJObjectInJToken(beerData,"style","abvMin",false),
                    AbvMax = StringFromJObjectInJToken(beerData,"style","abvMax",false),
                    SrmMin = StringFromJObjectInJToken(beerData,"style","srmMin",false),
                    SrmMax = StringFromJObjectInJToken(beerData,"style","srmMax",false),
                    OgMin = StringFromJObjectInJToken(beerData,"style","ogMin",false),
                    OgMax = StringFromJObjectInJToken(beerData,"style","ogMax",false),
                    FgMin = StringFromJObjectInJToken(beerData,"style","fgMin",false),
                    FgMax = StringFromJObjectInJToken(beerData,"style","fgMax",false)
                }                
            };
            
            result.Success = true;
            result.Message = $"Succesfully found {result.Beer.Name} information!";

            return result;
        }
        
        public async Task<BeerApiServiceResult> GetBeersFromBrewery(string breweryId)
        {
            var result = new BeerApiServiceResult()
            {
                Success = false,
                Message = "Undetermined failure while looking up for brewery beers!"
            };
            
            var results = await GetJsonFromApi($"brewery/{breweryId}?key=");
            
            if(results.Value<string>("status") != "success")
            {
                return result;
            }
            
            JToken breweryData;
            try
            {
                breweryData = results.Value<JToken>("data");
            }
            catch
            {
                return result;
            }
            
            result.Brewery = new Brewery
            {                
                Name = StringFromJToken(breweryData,"name"),
                Description = StringFromJToken(breweryData, "description"),
                Website = StringFromJToken(breweryData, "website"),
                Established = StringFromJToken(breweryData, "established"),
                Banner = StringFromJObjectInJToken(breweryData,"images","large",true)
            };
            
            results = await GetJsonFromApi($"brewery/{breweryId}/beers?key=");

            if (results.Value<string>("status") != "success")
            {
                return result;
            }

            result.Beers = new List<Beer>();

            var data = new JEnumerable<JToken>();

            try
            {
               data  = results["data"].Children();
            }
            catch
            {
                return result;
            }            
            
            var totalResults = 0;

            foreach(var item in data)
            {
                var beer = new Beer
                {                    
                    Name = StringFromJToken(item,"name"),
                    IdApi = StringFromJToken(item, "id"),
                    Icon = StringFromJObjectInJToken(item,"labels","icon",true),
                    Abv = StringFromJToken(item, "abv"),
                    Style = new Style
                    {
                        ShortName = StringFromJObjectInJToken(item, "style", "shortName", false)
                    }
                };

                result.Beers.Add(beer);
                totalResults++;
            }

            result.Message = $"Found {totalResults} results";
            result.Success = true;

            return result;
        }

        public async Task<BeerApiServiceResult> GetBeers(string beerName, int pageNum)
        {
            var result = new BeerApiServiceResult()
            {
                Success = false,
                Message = "Undetermined failure while looking up for beers!"
            };

            result.Beers = new List<Beer>();
            
            var results = await GetJsonFromApi($"beers/?name=*{beerName}*&p={pageNum}&withBreweries=Y&key=");

            bool foundData = true;
            int maxPageNum;

            try
            {
                maxPageNum = (int)results["numberOfPages"];
            }
            catch
            {
                foundData = false;
                maxPageNum = -1;
            }
            
            var validMaxPageNum = (pageNum > 0 && pageNum <= maxPageNum);

            if (foundData && validMaxPageNum)
            {
                var data = results["data"].Children();

                var totalResults = StringFromJToken(results,"totalResults");

                foreach (var item in data)
                {          
                   string breweryName;
                    try
                    {
                        var brewery = item.Value<JArray>("breweries");
                        breweryName = brewery[0].Value<string>("nameShortDisplay");                        
                    }
                    catch
                    {
                        breweryName = NOT_AVAILABLE;
                    }                 

                    var beer = new Beer()
                    {
                        Name = StringFromJToken(item, "nameDisplay"),
                        IdApi = StringFromJToken(item, "id"),
                        Abv = StringFromJToken(item, "abv"),
                        Icon = StringFromJObjectInJToken(item,"labels","icon",true),
                        Style = new Style
                        {
                            ShortName = StringFromJObjectInJToken(item,"style","shortName",false)
                        },
                        Brewery = new Brewery
                        {
                            Name = breweryName
                        }
                    };

                    result.Beers.Add(beer);
                }

                result.Message = $"Found {totalResults} results for \"{beerName}\"";
                result.Success = true;
                result.PageNumber = pageNum;
                result.MaxPageNumber = maxPageNum;
            }
            else
            {
                result.PageNumber = 1;
                if (foundData)
                {
                    result.Message = "Invalid page number!";
                }
                else
                {
                    result.Message = $"Could not find \"{beerName}\" please try another name";
                }                
            }
            
            return result;
        }

       public async Task<BeerApiServiceResult> GetBreweries(string breweryName, int pageNum)
        {
            var result = new BeerApiServiceResult()
            {
                Success = false,
                Message = "Undetermined failure while looking up for breweries!"
            };

            result.Breweries = new List<Brewery>();
            
            var results = await GetJsonFromApi($"breweries/?name=*{breweryName}*&p={pageNum}&key=");
            
            bool foundData = true;
            int maxPageNum;
            try
            {
                maxPageNum = (int)results["numberOfPages"];
            }
            catch 
            {
                foundData = false;
                maxPageNum = -1;
            }

            var validMaxPageNum = (pageNum > 0 && pageNum <= maxPageNum);

            if (foundData && validMaxPageNum)
            {
                var data = results["data"].Children();
                
                var totalResults = StringFromJToken(results,"totalResults");

                foreach (var item in data)
                {   
                    var brewery = new Brewery()
                    {
                        IdApi = StringFromJToken(item,"id"),
                        Name = StringFromJToken(item, "name"),
                        Image = StringFromJObjectInJToken(item, "images", "squareMedium", true)
                    };
                    
                    result.Breweries.Add(brewery);
                }
                
                result.Message = $"Found {totalResults} results for \"{breweryName}\"";
                result.Success = true;
                result.PageNumber = pageNum;
                result.MaxPageNumber = maxPageNum;
            }
            else
            {
                result.PageNumber = 1;
                if (foundData)
                {
                    result.Message = "Invalid page number!";
                }
                else
                {   
                    result.Message = $"Could not find \"{breweryName}\" please try another name";
                }
            }
            return result;
        }

        public async Task<BeerApiServiceResult> GetLocation(string locationId)
        {
            var result = new BeerApiServiceResult()
            {
                Success = false,
                Message = "Undetermined failure while looking for locations!"
            };

            var results = await GetJsonFromApi($"location/{locationId}?key=");

            if (results.Value<string>("status") != "success")
            {
                return result;
            }

            JToken locationData;
            try
            {
                locationData = results.Value<JToken>("data");
            }
            catch
            {
                return result;
            }

            JToken breweryData;
            try
            {
                breweryData = locationData.Value<JToken>("brewery");
            }
            catch
            {
                breweryData = null;
            }

            result.Location = new Location()
            {
                StreetAddress = StringFromJToken(locationData, "streetAddress"),
                IdApi = StringFromJToken(locationData, "id"),
                City = StringFromJToken(locationData, "locality"),
                State = StringFromJToken(locationData, "region"),
                PostalCode = StringFromJToken(locationData, "postalCode"),
                PhoneNumber = StringFromJToken(locationData, "phone"),
                HoursOfOperation = StringFromJToken(locationData, "hoursOfOperation"),
                Country = StringFromJObjectInJToken(locationData, "country", "displayName", false),
                Website = StringFromJToken(locationData, "website"),
                LocationType = StringFromJToken(locationData, "locationTypeDisplay"),
                Latitude = StringFromJToken(locationData, "latitude"),
                Longitude = StringFromJToken(locationData, "longitude"),
                Name = StringFromJToken(breweryData, "name"),
                Brewery = new Brewery
                {
                    Name = StringFromJToken(breweryData, "nameShortDisplay"),
                    Banner = StringFromJObjectInJToken(breweryData, "images", "large", true),
                    Website = StringFromJToken(breweryData, "website"),
                    Established = StringFromJToken(breweryData, "established"),
                    IdApi = StringFromJToken(breweryData, "id"),
                    Description = StringFromJToken(breweryData, "description"),
                    Image = StringFromJObjectInJToken(breweryData, "images", "squareMedium", true)
                },

            };

            result.Success = true;

            result.Message = $"Succesfully found {result.Location.Brewery.Name} information!";

            return result;
        }

        public async Task<BeerApiServiceResult> GetLocations(string region, string locality, int pageNum)
        {
            var result = new BeerApiServiceResult()
            {
                Success = false,
                Message = "Undetermined failure while looking for locations!"
            };
            var searchQuery = $"region={region}";
            if (!string.IsNullOrEmpty(locality))
            {
                searchQuery += $"&locality={locality}";
            }

            result.Locations = new List<Location>();
            var results = await GetJsonFromApi($"locations?isPrimary=Y&{searchQuery}&p={pageNum}&key=");
            
            bool foundData = true;
            int maxPageNum;
            try
            {
                maxPageNum = (int)results["numberOfPages"];
            }
            catch
            {
                foundData = false;
                maxPageNum = -1;
            }

            var validMaxPageNum = (pageNum > 0 && pageNum <= maxPageNum);

            if (foundData && validMaxPageNum)
            {
                var data = results["data"].Children();

                var totalResults = StringFromJToken(results, "totalResults");

                foreach (var item in data)
                {
                    var location = new Location()
                    {
                        IdApi = StringFromJToken(item, "id"),
                        StreetAddress = StringFromJToken(item, "streetAddress"),
                        City = StringFromJToken(item, "locality"),
                        State = StringFromJToken(item, "region"),
                        Country = StringFromJObjectInJToken(item, "country", "displayName", false),
                        Brewery = new Brewery(),
                        LocationType = StringFromJToken(item, "locationTypeDisplay")

                    };
                    try
                    {
                        var breweryData = item.Value<JToken>("brewery");
                        location.Brewery.Name = StringFromJToken(breweryData, "nameShortDisplay");
                        location.Brewery.Image = StringFromJObjectInJToken(breweryData, "images", "squareMedium", true);
                    }
                    catch
                    {
                        location.Brewery.Name = NOT_AVAILABLE;
                        location.Brewery.Image = NO_IMG;
                    }

                    result.Locations.Add(location);
                }
                var city = string.IsNullOrEmpty(locality) ? "" : locality + ", ";
                result.Message = $"Found {totalResults} results for \"{city}{region}\"";
                result.Success = true;
                result.PageNumber = pageNum;
                result.MaxPageNumber = maxPageNum;
            }
            else
            {
                result.PageNumber = 1;
                if (foundData)
                {
                    result.Message = "Invalid page number!";
                }
                else
                {
                    var city = string.IsNullOrEmpty(locality) ? "" : locality + ", ";
                    result.Message = $"Could not find any results for \"{city}{region}\" please try another search";
                }
            }

            return result;
        }
    }
}
