using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WHWebApp.ViewModels.BeersViewModels;
using System;
using WHWebApp.Models;
using WHWebApp.Services;
using WHWebApp.Data;
using WHWebApp.Utilities;

namespace WHWebApp.Controllers.Web
{
    [Authorize]
    public class BeersController : Controller
    {

        private readonly UserManager<ApplicationUser> _userManager;
        private ILogger _logger;
        private BeerApiService _beerApiService;
        private IAppRepository _repository;

        public BeersController(
        UserManager<ApplicationUser> userManager,
        ILoggerFactory loggerFactory,
        BeerApiService beerApiService,
        IAppRepository repository)
        {
            _userManager = userManager;
            _logger = loggerFactory.CreateLogger<BeersController>();
            _beerApiService = beerApiService;
            _repository = repository;
        }

        //
        //GET: /Beers/DeleteBeer?BeerId=
        [HttpGet]
        public IActionResult DeleteBeer(string BeerId, string BeerName)
        {
            var beerId = Extensions.SanitizeString(BeerId);
            var beerName = Extensions.SanitizeString(BeerName);

            var deleteBeerViewModel = new DeleteBeerViewModel()
            {
                BeerId = beerId,
                BeerName = beerName,
                Success = true,
                Message = $"Would you like to delete {beerName} from your list?",
                Beer = new Beer(),
                Deleted = false
            };

            return View(deleteBeerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteBeer(DeleteBeerViewModel deleteBeerViewModel)
        {
            var beerId = Extensions.SanitizeString(deleteBeerViewModel.BeerId);
            var userName = User.Identity.Name;
            var beerName = Extensions.SanitizeString(deleteBeerViewModel.BeerName);

            deleteBeerViewModel.Success = _repository.DeleteBeer(beerId, userName);
            deleteBeerViewModel.Deleted = true;
            deleteBeerViewModel.BeerName = beerName;
            deleteBeerViewModel.BeerId = beerId;

            if (deleteBeerViewModel.Success)
            {
                deleteBeerViewModel.Message = $"{beerName} was deleted succesfully from your list.";
            }
            else
            {
                deleteBeerViewModel.Message = $"{beerName} is not in your list or we had trouble deleting it.";
            }

            var user = await GetCurrentUserAsync();
            deleteBeerViewModel.UserFirstName = user.FirstName;

            return View(deleteBeerViewModel);
        }

        //
        //GET: /Beers/AddBeer?BeerId=
        [HttpGet]
        public IActionResult AddBeer(string BeerId, string BeerName)
        {   
            var beerId = Extensions.SanitizeString(BeerId);
            var beerName = Extensions.SanitizeString(BeerName);
            
            var addBeerViewModel = new AddBeerViewModel()
            {
                BeerId = beerId,
                BeerName = beerName,
                Success = true,
                Message = $"Would you like to add {beerName} to your list?",
                Beer = new Beer(),
                Post = false
            };
                        
            return View(addBeerViewModel);
        }

        //
        //POST: /Beers/AddBeer?BeerId=
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBeer(AddBeerViewModel addBeerViewModel)
        {
            var beerId = Extensions.SanitizeString(addBeerViewModel.BeerId);

            var beerApiResult = await _beerApiService.GetBeerInformation(beerId);

            addBeerViewModel.Success = beerApiResult.Success;
            addBeerViewModel.Message = beerApiResult.Message;
            addBeerViewModel.Beer = beerApiResult.Beer;
            addBeerViewModel.Post = true;
            addBeerViewModel.Beer.UserName = User.Identity.Name;
            addBeerViewModel.BeerName = addBeerViewModel.Beer.Name;            
            
            
            if(!beerApiResult.Success)
            {
                return View(addBeerViewModel);
            }            
            var user = await GetCurrentUserAsync();
            addBeerViewModel.UserFirstName = user.FirstName;

            try
            {
                if(_repository.AddBeer(addBeerViewModel.Beer,User.Identity.Name)
                    && _repository.SaveAll())
                {
                    addBeerViewModel.Message = $"{addBeerViewModel.Beer.Name} was succesfully added!";
                    addBeerViewModel.Success = true;                   
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                addBeerViewModel.Success = false;
                addBeerViewModel.Message = $"{addBeerViewModel.Beer.Name} is already in your list or we had trouble adding it.";                
            }

            return View(addBeerViewModel);
        }

        //
        //GET: /Beers/Index
        [HttpGet]
        public async Task<IActionResult> Index(){
            
            var user = await GetCurrentUserAsync();
            
            var viewModel = new IndexViewModel
            { 
                FirstName = Extensions.SanitizeString(user.FirstName)
            };
            
            return View(viewModel);
        }

        //
        //GET: /App/BeerInfo?BeerId=
        public async Task<IActionResult> BeerInfo(string BeerId)
        {
            if (string.IsNullOrEmpty(BeerId))
            {
                return RedirectToAction(nameof(Index));
            }

            var beerId = Extensions.SanitizeString(BeerId);

            var breweryApiResult = await _beerApiService.GetBeerInformation(beerId);

            var beerInfoViewModel = new BeerInfoViewModel
            {
                Message = breweryApiResult.Message,
                Success = breweryApiResult.Success,
                Beer = breweryApiResult.Beer
            };

            return View(beerInfoViewModel);
        }

        //
        //GET: /Beers/Beer
        public async Task<IActionResult> SearchBeer(string search,string pagenum)
        {
            if (string.IsNullOrEmpty(search) || string.IsNullOrEmpty(pagenum))
            {
                return RedirectToAction(nameof(Index));
            }

            var beerName = Extensions.SanitizeString(search);

            int pageNumber;
            if(!int.TryParse(pagenum,out pageNumber))
            {
                pageNumber = 1;                
            }

            if(pageNumber < 1)
            {
                pageNumber = 1;
            }

            if (beerName != string.Empty)
            {
                var beerViewModel = new SearchBeerViewModel()
                {
                    SearchName = beerName,                    
                    Beers = new List<Beer>()
                };

                var beerApiResult = await _beerApiService.GetBeers(beerName,pageNumber);

                beerViewModel.Message = beerApiResult.Message;
                beerViewModel.PageNumber = beerApiResult.PageNumber;
                beerViewModel.Success = beerApiResult.Success;
                beerViewModel.MaxPageNumber = beerApiResult.MaxPageNumber;
                
                if (beerApiResult.Success)
                {
                    foreach (var beer in beerApiResult.Beers)
                    {
                        beerViewModel.Beers.Add(beer);
                    }
                }

                return View(beerViewModel);
            }

            return RedirectToAction(nameof(Index));
        }      

        //
        //GET: /App/SarchBrewery
        public async Task<IActionResult> SearchBrewery(string search, string pagenum)
        {
            if (string.IsNullOrEmpty(search) || string.IsNullOrEmpty(pagenum))
            {
                return RedirectToAction(nameof(Index));
            }

            var breweryName = Extensions.SanitizeString(search);

            int pageNumber;
            if (!int.TryParse(pagenum, out pageNumber))
            {
                pageNumber = 1;
            }

            if(pageNumber < 1)
            {
                pageNumber = 1;
            }

            var breweryApiResult = await _beerApiService.GetBreweries(breweryName, pageNumber);

            var brewerySearchModel = new SearchBreweryViewModel()
            {
                SearchName = breweryName,
                MaxPageNumber = breweryApiResult.MaxPageNumber,
                Message = breweryApiResult.Message,
                PageNumber = breweryApiResult.PageNumber,
                Success = breweryApiResult.Success,
                Breweries = new List<Brewery>()
            };

            if (breweryApiResult.Success)
            {
                foreach(var brewery in breweryApiResult.Breweries)
                {
                    brewerySearchModel.Breweries.Add(brewery);
                }
            }

            return View(brewerySearchModel);
        }

         //
        //GET: /App/Brewery?BreweryId=
        public async Task<IActionResult> Brewery(string BreweryId)
        {
            if (string.IsNullOrEmpty(BreweryId))
            {
                return RedirectToAction(nameof(Index));
            }
            
            var breweryId = Extensions.SanitizeString(BreweryId);

            var breweryApiResult = await _beerApiService.GetBeersFromBrewery(breweryId);

            var breweryViewModel = new BreweryViewModel
            {
                Brewery = new Brewery(),
                Beers = new List<Beer>(),
                Message = breweryApiResult.Message,
                Success = breweryApiResult.Success
            };

            if (breweryApiResult.Success)
            {
                breweryViewModel.Brewery = breweryApiResult.Brewery;

                foreach(var beer in breweryApiResult.Beers)
                {
                    breweryViewModel.Beers.Add(beer);
                }
            }
            return View(breweryViewModel);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            AddLoginSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        #endregion
    }
}