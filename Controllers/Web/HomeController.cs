using Microsoft.AspNetCore.Mvc;

namespace WHWebApp.Controllers.Web
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
   
        public IActionResult Error()
        {
            return View();
        }
        
    }
}
