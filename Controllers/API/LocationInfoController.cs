﻿using AutoMapper;
using WHWebApp.Data;
using WHWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;

namespace WHWebApp.Controllers.API
{
    [Authorize]
    [Route("api/location/{locationId}")]
    public class LocationInfoController : Controller
    {
        private ILogger _logger;
        private IAppRepository _repository;

        public LocationInfoController(IAppRepository repository,
        ILogger<LocationInfoController> logger)
        {
            _logger = logger;
            _repository = repository;
        }

        //GET: api/location/{locationId}
        //This method gets beer data from the db
        [HttpGet("")]
        public JsonResult Get(string locationId)
        {
            try
            {
                var id = Utilities.Extensions.SanitizeString(locationId);
                var location = _repository.GetLocationById(id, User.Identity.Name);

                var result = Mapper.Map<Location>(location);

                if (result == null)
                {
                    return Json(null);
                }

                return Json(result);
            }
            catch
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Error ocurred finiding the beer information");
            }
        }
    }
}
