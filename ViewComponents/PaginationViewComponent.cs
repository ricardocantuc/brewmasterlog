using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WHWebApp.ViewModels.ViewComponentModels;

namespace WHWebApp.ViewComponents
{
    public class PaginationViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(int pageNumber, int maxPageNumber, string controller, string action, string searchName)
        {
            var paginationViewModel = new PaginationViewComponentModel
            {
                PageNumber = pageNumber,
                MaxPageNumber = maxPageNumber,
                Controller = controller,
                Action = action,
                SearchName = searchName
            };

            return View(paginationViewModel);
        }
    }
}